import { Config } from 'protractor';

export const config: Config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['tests/*'],
    capabilities : {
        'browserName' : 'chrome',
    },
   directConnect : true,
}