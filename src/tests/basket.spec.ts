import { browser } from 'protractor';
import { BasketPage } from '../pages/basketPage';
import { HomepagePage } from '../pages/homepagePage';

describe ('Testing basket', () => {

    const basketPage = new BasketPage();
    const homepagePage = new HomepagePage();

    beforeEach(() => {
        browser.get('https://www.tescomobile.com/');
    })

    it('check basket icon', () => {
        expect(basketPage.basketIcon.isPresent()).toBe(false);
    });

    it('trying to access empty basket', () => {
        browser.get('https://www.tescomobile.com/shop/basket');
        expect(browser.getCurrentUrl()).toEqual('https://www.tescomobile.com/shop/empty-basket');
        expect(basketPage.emptyBasketInfo.isPresent());
    });

    it('remove item from basket', () => {
        homepagePage.goToiPhone11();
        browser.actions().mouseMove(basketPage.chooseUsage).perform();
        basketPage.chooseUsage.click();
        basketPage.chooseTariff.click();
    })

});