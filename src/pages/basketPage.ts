import { browser, element, by, $ } from 'protractor';

export class BasketPage {
    public basketIcon = $('.main-header__basket.main-header__tools--icon');
    public emptyBasketInfo = $('.grid grid--uniform.basket-cta.basket-empty box');
    public chooseUsage = element(by.id('analytics-pdp-choose-tariff'));
    public chooseTariff = $('a.button');
}