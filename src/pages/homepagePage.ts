import  {browser, element, by, protractor, $ } from 'protractor';

export class HomepagePage {
    public basketCounter = $('.main-header__tools__item');
    public ip11Header = $('.//span[contains(text(), "iPhone 11")]');
    public searchResult = element(by.id('page-title-component'));
    public mainLogo = $('a.main-header__logo');
    public navBar = element(by.id('analytics-header-menu'));
    public dealCard10 = element(by.xpath('.//*[app-featured-deal-card]//div[contains(text(), "10GB")]'));
    public zeroBuffer = element(by.id('analytics-tariff-bill-cap-00000'));
    public cookiesButton = element(by.id('tesco_cookie_widget'));

    private navShop = element(by.xpath('.//*[li/a[contains(text(), "Shop")]]/li[3]'));
    private iphone11Link = element(by.xpath('.//*[@href="https://www.tescomobile.com/shop/apple/iphone-11"]'));
    private searchInput = element(by.id('analytics-header-search'));
    private simOnly = element(by.buttonText('Shop SIM Only deals'));
    private addToBasket = element(by.id('analytics-tariff-cta-view-your-basket'));

   public goToiPhone11() {
        this.navShop.click();
        this.iphone11Link.click();
    }

    public searchByInput() {
       this.searchInput.sendKeys('iphone');
       this.searchInput.sendKeys(protractor.Key.ENTER);
    }

    public chooseSimOnlyOffer() {
        browser.actions().mouseMove(this.simOnly).perform();
        this.simOnly.click();
    }

    public addtoBasket() {
        this.addToBasket.click();
    }
}