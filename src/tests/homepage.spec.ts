import { browser } from 'protractor';
import { HomepagePage } from '../pages/homepagePage';

describe('Tesco mobile', () => {
        
        const homepagePage = new HomepagePage();

        beforeEach(() => {
            browser.get('https://www.tescomobile.com/');
        })

        it('homepage test', () => {
            homepagePage.mainLogo.isPresent();
            expect(homepagePage.navBar.isPresent());
        });

        it('close cookies info', () => {
            homepagePage.cookiesButton.click();
            expect((homepagePage.cookiesButton).isPresent()).toBe(false);
        })

        it('search for iphone', () => {
            homepagePage.searchByInput();
            expect(homepagePage.searchResult.getText()).toEqual('Search results for iphone');
        });

        it('go to iphone 11', () => {
            homepagePage.goToiPhone11();
            expect(homepagePage.ip11Header.getText()).toEqual('Apple iPhone 11');
        });

        it('add sim only to basket'), () => {
            homepagePage.chooseSimOnlyOffer()
            homepagePage.dealCard10.click();
            homepagePage.zeroBuffer.click();
            homepagePage.addtoBasket();
            expect(browser.getCurrentUrl()).toEqual('https://www.tescomobile.com/shop/basket');
            expect(homepagePage.basketCounter.getText()).toEqual('1');
        };
});